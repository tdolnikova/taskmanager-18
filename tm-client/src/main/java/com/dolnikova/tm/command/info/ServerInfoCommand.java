package com.dolnikova.tm.command.info;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.ServerInfo;
import org.jetbrains.annotations.NotNull;

public class ServerInfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.SERVER_INFO;
    }

    @NotNull
    @Override
    public String description() {
        return Command.SERVER_INFO_DESCRIPTION;
    }

    @Override
    public void execute() {
        String port = "NOT FOUND";
        String host = "NOT FOUND";
        ServerInfo serverInfo = serviceLocator.getSessionEndpoint().getServerInfo();
        if (serverInfo != null) {
            host = serverInfo.getHost();
            port = serverInfo.getPort();
        }
        System.out.println(
                "[server-info]\n" +
                "HOST: " + host + "\n" +
                "PORT: " + port);
    }

    @Override
    public boolean isSecure() {
        return true;
    }

}
