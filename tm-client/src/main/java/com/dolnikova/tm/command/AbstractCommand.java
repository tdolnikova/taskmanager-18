package com.dolnikova.tm.command;

import com.dolnikova.tm.api.EndpointLocator;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractCommand {

    @NotNull
    public EndpointLocator serviceLocator;
    public void setServiceLocator(@NotNull final EndpointLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
    @NotNull public abstract String command();
    @NotNull public abstract String description();
    public abstract void execute() throws Exception;
    public abstract boolean isSecure();
}