package com.dolnikova.tm;

import com.dolnikova.tm.bootstrap.Bootstrap;

import javax.enterprise.inject.se.SeContainerInitializer;

public final class AppServer {

    public static void main(String[] args) {
        SeContainerInitializer.newInstance()
                .addPackages(AppServer.class).initialize()
                .select(Bootstrap.class).get()
                .init();
    }

}