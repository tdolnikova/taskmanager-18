package com.dolnikova.tm.util;

import com.dolnikova.tm.dto.ProjectDTO;
import com.dolnikova.tm.dto.SessionDTO;
import com.dolnikova.tm.dto.TaskDTO;
import com.dolnikova.tm.dto.UserDTO;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DtoConversionUtil {

    @Nullable
    public static Session dtoToSession(@Nullable final SessionDTO sessionDTO, @Nullable final User user) {
        if (sessionDTO == null) return null;
        @NotNull final Session session = new Session();
        session.setId(sessionDTO.getId());
        session.setSignature(sessionDTO.getSignature());
        session.setTimestamp(sessionDTO.getTimestamp());
        session.setUser(user);
        return session;
    }

    @Nullable
    public static SessionDTO sessionToDto(@Nullable final Session session) {
        if (session == null) return null;
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(session.getId());
        sessionDTO.setSignature(session.getSignature());
        sessionDTO.setTimestamp(session.getTimestamp());
        sessionDTO.setUserId(session.getUser().getId());
        return sessionDTO;
    }

    @Nullable
    public static User dtoToUser(@Nullable final UserDTO userDTO) {
        if (userDTO == null) return null;
        @NotNull final User user = new User();
        user.setId(userDTO.getId());
        user.setEmail(userDTO.getEmail());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setLogin(userDTO.getLogin());
        user.setMiddleName(userDTO.getMiddleName());
        user.setPasswordHash(userDTO.getPasswordHash());
        user.setPhone(userDTO.getPhone());
        user.setLocked(userDTO.isLocked());
        user.setRole(userDTO.getRole());
        return user;
    }

    @Nullable
    public static UserDTO userToDto(@Nullable final User user) {
        if (user == null) return null;
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setEmail(user.getEmail());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setLogin(user.getLogin());
        userDTO.setMiddleName(user.getMiddleName());
        userDTO.setPasswordHash(user.getPasswordHash());
        userDTO.setPhone(user.getPhone());
        userDTO.setLocked(userDTO.isLocked());
        userDTO.setRole(user.getRole());
        return userDTO;
    }

    @Nullable
    public static Project dtoToProject(@Nullable final ProjectDTO projectDTO, @Nullable final User user) {
        if (projectDTO == null) return null;
        @NotNull final Project project = new Project();
        project.setId(projectDTO.getId());
        project.setUser(user);
        project.setCreationDate(projectDTO.getCreationDate());
        project.setDateBegin(projectDTO.getDateBegin());
        project.setDateEnd(projectDTO.getDateEnd());
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setStatus(projectDTO.getStatus());
        return project;
    }

    @Nullable
    public static ProjectDTO projectToDto(@Nullable final Project project) {
        if (project == null) return null;
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setUserId(project.getUser().getId());
        projectDTO.setCreationDate(project.getCreationDate());
        projectDTO.setDateBegin(project.getDateBegin());
        projectDTO.setDateEnd(project.getDateEnd());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setStatus(project.getStatus());
        return projectDTO;
    }

    @Nullable
    public static Task dtoToTask(@Nullable final TaskDTO taskDTO, @Nullable final Project project, @Nullable final User user) {
        if (taskDTO == null) return null;
        @NotNull final Task task = new Task();
        task.setId(taskDTO.getId());
        task.setCreationDate(taskDTO.getCreationDate());
        task.setDateBegin(taskDTO.getDateBegin());
        task.setDateEnd(taskDTO.getDateEnd());
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setProject(project);
        task.setUser(user);
        task.setStatus(taskDTO.getStatus());
        return task;
    }

    @Nullable
    public static TaskDTO taskToDto(@Nullable final Task task) {
        if (task == null) return null;
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setCreationDate(task.getCreationDate());
        taskDTO.setDateBegin(task.getDateBegin());
        taskDTO.setDateEnd(task.getDateEnd());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        @Nullable final Project project = task.getProject();
        if (project != null) taskDTO.setProjectId(project.getId());
        else System.out.println("Не найден проект");
        @Nullable final User user = task.getUser();
        if (user != null) taskDTO.setUserId(task.getUser().getId());
        else System.out.println("Не найдена задача");
        taskDTO.setStatus(task.getStatus());
        return taskDTO;
    }

}
