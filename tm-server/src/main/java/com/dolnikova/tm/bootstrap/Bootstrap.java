package com.dolnikova.tm.bootstrap;

import com.dolnikova.tm.endpoint.ProjectEndpoint;
import com.dolnikova.tm.endpoint.SessionEndpoint;
import com.dolnikova.tm.endpoint.TaskEndpoint;
import com.dolnikova.tm.endpoint.UserEndpoint;
import com.hazelcast.core.Hazelcast;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.ws.Endpoint;
import java.util.Scanner;

@Getter
@Setter
@Singleton
@NoArgsConstructor
public final class Bootstrap {

    @Inject
    private UserEndpoint userEndpoint;
    @Inject
    private TaskEndpoint taskEndpoint;
    @Inject
    private ProjectEndpoint projectEndpoint;
    @Inject
    private SessionEndpoint sessionEndpoint;

    @Getter @Nullable static String host;
    @Getter @Nullable static String port;

    public void init() {
        Hazelcast.newHazelcastInstance();
        host = "localhost";
        port = System.getProperty("server.port");
        if (port == null || port.isEmpty()) {
            port = "8080";
        }
        @NotNull final String address = "http://" + host + ":";
        Endpoint.publish(address + port + "/TaskEndpoint?WSDL", taskEndpoint);
        System.out.println(address + port + "/TaskEndpoint?WSDL");
        Endpoint.publish(address + port + "/ProjectEndpoint?WSDL", projectEndpoint);
        System.out.println(address + port + "/ProjectEndpoint?WSDL");
        Endpoint.publish(address + port + "/UserEndpoint?WSDL", userEndpoint);
        System.out.println(address + port + "/UserEndpoint?WSDL");
        Endpoint.publish(address + port + "/SessionEndpoint?WSDL", sessionEndpoint);
        System.out.println(address + port + "/SessionEndpoint?WSDL");

        Scanner scanner = new Scanner(System.in);
        String input = "";
        while (!"exit".equals(input)) {
            try {
                input = scanner.nextLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.exit(0);
    }

}
