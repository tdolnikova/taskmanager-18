package com.dolnikova.tm.server;

import com.dolnikova.tm.bootstrap.Bootstrap;

import javax.enterprise.inject.Produces;

public class ServerInfoFactory {

    @Produces
    ServerInfo produceServerInfo()  {
        System.out.println("[RECEIVING SERVER INFO]");
        ServerInfo serverInfo = new ServerInfo();
        serverInfo.setHost(Bootstrap.getHost());
        serverInfo.setPort(Bootstrap.getPort());
        return serverInfo;
    }

}