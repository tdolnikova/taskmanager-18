package com.dolnikova.tm.service;

import com.dolnikova.tm.api.service.IService;
import com.dolnikova.tm.entity.AbstractEntity;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

}
