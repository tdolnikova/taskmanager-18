package com.dolnikova.tm.entity;

import com.dolnikova.tm.enumerated.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "app_project")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Project extends AbstractEntity implements Serializable {

    private Date dateBegin;

    private Date dateEnd;

    private String name;

    private String description;

    @ManyToOne(targetEntity = User.class)
    private User user;

    @Column(updatable = false)
    private Date creationDate = new Date();

    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

}
