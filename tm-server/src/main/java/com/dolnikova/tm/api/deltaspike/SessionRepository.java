package com.dolnikova.tm.api.deltaspike;

import com.dolnikova.tm.entity.Session;
import org.apache.deltaspike.data.api.FullEntityRepository;
import org.apache.deltaspike.data.api.Modifying;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jetbrains.annotations.Nullable;

import javax.persistence.QueryHint;
import java.util.Collection;

@Repository(forEntity = Session.class)
public interface SessionRepository extends FullEntityRepository<Session, String> {

    @Query(value = "select s from Session s where s.id = ?1", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    Session findSessionById(@Nullable String id);

    @Query(value = "select s from Session s where s.user.id = ?1", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    Session findSessionByUserId(@Nullable String userId);

    @Query(value = "select s from Session s where s.signature = ?1", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    Session findSessionBySignature(@Nullable String signature);

    @Query(value = "select s from Session s where s.user.id = ?1", hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    Collection<Session> findAllByUserId(@Nullable String userId);

    @Modifying
    @Transactional
    @Query("delete from Session s where s.user.id = ?1")
    void deleteAllByUserId(@Nullable String userId);

}
