package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ITaskService extends IService<Task> {

    @Nullable
    Task findOneById(final @Nullable String ownerId, final @Nullable String id);

    Task findOneByName(final @Nullable String name, final @Nullable String ownerId);

    @Nullable Collection<Task> findAll(final @Nullable String ownerId);

    @Nullable Collection<Task> findAllByName(@Nullable String ownerId, @Nullable String text);

    @Nullable Collection<Task> findAllByDescription(@Nullable String ownerId, @Nullable String text);

    @Override
    void persist(final @Nullable Task entity);

    void persistList(final @Nullable Collection<Task> list);

    void merge(final @Nullable String newData, final @Nullable Task entityToMerge, final @Nullable DataType dataType);

    @Override
    void remove(final @Nullable Task entity);

    void removeAll(final @Nullable String ownerId);

}
