package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.AbstractEntity;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IService<E extends AbstractEntity> {

    void persist(@Nullable final E entity);

    void persistList(@Nullable final Collection<E> list);

    void remove(@Nullable final E entity);

}
