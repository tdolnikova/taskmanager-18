package com.dolnikova.tm.enumerated;

public enum Role {

    ADMIN("АДМИНИСТРАТОР"),
    USER("ОБЫЧНЫЙ ПОЛЬЗОВАТЕЛЬ");

    private final String name;

    Role(final String name) {
        this.name = name;
    }

    public String displayName() {
        return name;
    }

}
