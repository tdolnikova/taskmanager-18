package com.dolnikova.tm;

import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.service.ProjectService;
import com.dolnikova.tm.service.UserService;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

@RunWith(CdiTestRunner.class)
public class ProjectTest {

    @Inject
    ProjectService projectService;

    @Inject
    UserService userService;

    private User user;

    @Before
    public void startTest() {
        System.out.println("[START]");
        user = new User();
        user.setLogin("123");
        user.setPasswordHash("123");
        userService.persist(user);
        user = userService.findOneByLogin("123");
        Assert.assertNotNull(user);
    }

    @After
    public void finishTest() {
        System.out.println("[FINISH]");
    }

    @Test
    public void persistAndFind() {
        Project project = new Project();
        project.setName("123");
        project.setDescription("123");
        project.setUser(user);
        projectService.persist(project);
        Project savedProjectName = projectService.findOneByName("123");
        Assert.assertNotNull(savedProjectName);
        Project savedProjectId = projectService.findOneById(user.getId(), savedProjectName.getId());
        Assert.assertNotNull(savedProjectId);
    }

}
